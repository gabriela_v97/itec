package com.itec.Domain;

import javax.persistence.*;

@Entity
@Table(name = "quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "leaderboard")
    private String leaderboard;

    public Quiz() {
        this.leaderboard = "";
    }

    public Quiz(String leaderboard, String questions) {
        this.leaderboard = leaderboard;
    }

    public Integer getId() {
        return id;
    }

    public String getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(String leaderboard) {
        this.leaderboard = leaderboard;
    }
}
