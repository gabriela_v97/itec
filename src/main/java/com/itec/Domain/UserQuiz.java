package com.itec.Domain;

import javax.persistence.*;

@Entity
@Table(name = "userQuiz")
public class UserQuiz {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "user")
    private Integer user;

    @Column (name = "quiz")
    private Integer quiz;

    public UserQuiz() {
        this.user = -1;
        this.quiz = -1;
    }

    public UserQuiz(Integer user, Integer quiz) {
        this.user = user;
        this.quiz = quiz;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getQuiz() {
        return quiz;
    }

    public void setQuiz(Integer quiz) {
        this.quiz = quiz;
    }
}
