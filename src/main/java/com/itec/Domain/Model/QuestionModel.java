package com.itec.Domain.Model;

import java.util.ArrayList;
import java.util.List;

public class QuestionModel {
    private String question;
    private List<AnswerModel> answers;
    private String category;
    private int score;
    private int difficultyLevel;

    public QuestionModel() {
        this.question = "";
        this.answers = new ArrayList<>();
        this.category = "";
        this.score = 0;
        this.difficultyLevel = 0;
    }

    public QuestionModel(String question, List<AnswerModel> answers, String category, int score, int difficultyLevel) {
        this.question = question;
        this.answers = answers;
        this.category = category;
        this.score = score;
        this.difficultyLevel = difficultyLevel;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<AnswerModel> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerModel> answers) {
        this.answers = answers;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(int difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }
}
