package com.itec.Domain.Model;

public class AnswerModel {

    private String answer;
    private int isCorrect;

    public AnswerModel() {
        this.answer = "";
        this.isCorrect = 0;
    }

    public AnswerModel(String answer, int isCorrect) {
        this.answer = answer;
        this.isCorrect = isCorrect;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }
}
