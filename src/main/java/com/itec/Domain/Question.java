package com.itec.Domain;

import javax.persistence.*;

@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "question")
    private String question;

    @Column(name = "answers")
    private String answers;

    @Column(name = "correctAnswers")
    private String correctAnswers;

    @Column (name = "category")
    private String category;

    @Column (name = "score")
    private Integer score;

    @Column (name  = "difficultyLevel")
    private Integer difficultyLevel;

    public Question() {
        this.question = "";
        this.answers = "";
        this.correctAnswers = "";
        this.category = "";
        this.score = -1;
        this.difficultyLevel = -1;
    }

    public Question(String question, String answers, String correctAnswers, String category, Integer score, Integer difficultyLevel) {
        this.question = question;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
        this.category = category;
        this.score = score;
        this.difficultyLevel = difficultyLevel;
    }

    public Integer getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public String getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(String correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(Integer difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }
}
