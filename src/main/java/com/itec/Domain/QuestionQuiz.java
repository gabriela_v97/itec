package com.itec.Domain;

import javax.persistence.*;

@Entity
@Table(name = "questionQuiz")
public class QuestionQuiz {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "question")
    private Integer idQuestion;

    @Column(name = "quiz")
    private Integer idQuiz;

    public QuestionQuiz() {
        this.idQuestion = -1;
        this.idQuiz = -1;
    }

    public QuestionQuiz(Integer idQuestion, Integer idQuiz) {
        this.idQuestion = idQuestion;
        this.idQuiz = idQuiz;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }

    public Integer getIdQuiz() {
        return idQuiz;
    }

    public void setIdQuiz(Integer idQuiz) {
        this.idQuiz = idQuiz;
    }
}
