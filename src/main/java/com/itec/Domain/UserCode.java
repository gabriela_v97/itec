package com.itec.Domain;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
@Table(name = "userCode")
public class UserCode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "user")
    private Integer user;

    @Column (name =  "code")
    private Integer code;

    public UserCode() {
        this.user = -1;
        this.code = 0;
    }

    public UserCode(Integer user, Integer code) {
        this.user = user;
        this.code = code;
    }
}
