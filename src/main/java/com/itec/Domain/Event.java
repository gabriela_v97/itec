package com.itec.Domain;


import javax.persistence.*;

@Entity
@Table(name = "event")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column (name = "status")
    private String status;

    @Column (name = "quizes")
    private String quizes;

    public Event() {
        this.name = "";
        this.status = "";
        this.quizes = "";
    }

    public Event(String name, String status, String quizes) {
        this.name = name;
        this.status = status;
        this.quizes = quizes;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuizes() {
        return quizes;
    }

    public void setQuizes(String quizes) {
        this.quizes += quizes + ";";
    }
}
