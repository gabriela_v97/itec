package com.itec.Controller;

import com.itec.Domain.Quiz;
import com.itec.Service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/quiz")
public class QuizController {
    @Autowired
    QuizService quizService;

    @RequestMapping(value = "/getQuizes")
    public List<Quiz> getAllQuizes(){
        return quizService.getAllQuizes();
    }
}
