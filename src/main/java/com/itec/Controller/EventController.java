package com.itec.Controller;

import com.itec.Domain.Event;
import com.itec.Domain.Quiz;
import com.itec.Service.EventService;
import com.itec.Service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/event")
public class EventController {
    @Autowired
    EventService eventService;

    @RequestMapping(value = "/getEvents")
    public List<Event> getAllEvents(){
        return eventService.getAllEvents();
    }
}
