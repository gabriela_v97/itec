package com.itec.Controller;

import com.itec.Domain.User;
import com.itec.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/add/{name}/{password}", method = RequestMethod.POST)
    public User addUser(@PathVariable("name") String name, @PathVariable("password") String password ) {

        return userService.saveNewUser(name, password);
    }

    @RequestMapping(value = "/update/{name}/{password}", method = RequestMethod.POST)
    public User updateUser(@PathVariable("name") String name, @PathVariable("password") String password){
        return  userService.updateUser(name, password);
    }

    @RequestMapping(value = "delete/{name}", method = RequestMethod.DELETE)
    public boolean deleteUser(@PathVariable("name") String name){
        return userService.deleteUser(name);
    }
}
