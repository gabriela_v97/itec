package com.itec.Service;

import com.itec.Domain.User;
import com.itec.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findUserByNameAndPassword(String email, String password){
        return userRepository.findByEmailAndPassword(email, password);
    }

    public User saveNewUser(String name, String password){
       /* User newUser = new User(name, password);
        User u = userRepository.save(newUser);
        System.out.println("user nou = " + u.getName() );
        return u;
        */
       return null;
    }

    public boolean deleteUser(String name){
     /*   User user = userRepository.findByName(name);
        if(user.getId() != 0){
            //user exists
            userRepository.delete(user);
            return true;
        }*/
        return false;
    }

    public User updateUser(String name, String password){
       /* User user = userRepository.findByName(name);
        if(user.getId() !=0){
            //user exists => update
            user.setName(name);
            user.setPassword(password);
            return userRepository.save(user);
        }*/
        return new User();
    }
}
