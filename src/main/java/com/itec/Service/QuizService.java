package com.itec.Service;

import com.itec.Domain.Quiz;
import com.itec.Repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizService {
    @Autowired
    QuizRepository quizRepository;

    public List<Quiz> getAllQuizes(){
        return quizRepository.findAll();
    }
}
